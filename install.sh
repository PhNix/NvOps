#!/usr/bin/bash

# Delete old configuration
rm -rf ~/.local/share/nvim
rm -rf ~/.config/nvim

# Copy configuration files in .config directory
mkdir -p ~/.config/nvim
cp -r ./* ~/.config/nvim

# Startup the neovim
nvim

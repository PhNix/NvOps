---------------------------------------------------------------------------
-- Notify - Notifications UI
---------------------------------------------------------------------------

local options = {
  background_colour = "#000000",
  enabled = false,
}

require("notify").setup(options)
vim.notify = require("notify")

---------------------------------------------------------------------------
-- Plugin: Status line
---------------------------------------------------------------------------

local options = {
  options = {
    icons_enabled = true,
    theme = "auto",
    component_separators = { left = "", right = "" },
    section_separators = { left = "", right = "" },
  },
  disabled_filetypes = {
    statusline = {},
    winbar = {},
  },
  refresh = {
    statusline = 1000,
    tabline = 1000,
    winbar = 1000,
  },
  globalstatus = true,
  sections = {
    lualine_x = {
      {
        require("noice").api.status.mode.get,
        cond = require("noice").api.status.mode.has,
        color = { fg = "#ff9e64" },
      },
      "filetype",
    },
    lualine_y = {
      "encoding",
      "fileformat",
    },
    lualine_z = {
      "progress",
    },
    lualine_a = {
      "branch",
    },
    lualine_b = {
      "filename",
    },
    lualine_c = {
      "diagnostics",
    },
  },
}

require("lualine").setup(options)

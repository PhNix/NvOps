---------------------------------------------------------------------------
-- Plugin: Trouble
---------------------------------------------------------------------------
local options = {}

local signs = {
  Error = " ",
  Warning = " ",
  Hint = " ",
  Information = " ",
}

for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

require("trouble").setup(options)

require("core.utils").load_mappings "trouble"

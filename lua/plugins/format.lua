---------------------------------------------------------------------------
-- Formatter and Linters integration
---------------------------------------------------------------------------

local merge_tb = vim.tbl_deep_extend

local linters = require("config").linters
require("lint").linters_by_ft = linters

vim.api.nvim_create_autocmd({ "BufWritePost" }, {
  callback = function()
    require("lint").try_lint()
  end,
})

local formatters = require("config").formatters
local options = {
  logging = true,
  log_level = vim.log.levels.WARN,
  filetype = merge_tb("force", {
    ["*"] = {
      require("formatter.filetypes.any").remove_trailing_whitespace,
    },
  }, formatters),
}
require("formatter").setup(options)

require("core.utils").load_mappings "format"

---------------------------------------------------------------------------
-- Comments system
---------------------------------------------------------------------------
require("Comment").setup {}
require("core.utils").load_mappings "comment"

require("todo-comments").setup {}

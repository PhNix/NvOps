---------------------------------------------------------------------------
-- Plugin: TreeSitter
---------------------------------------------------------------------------
local options = {
  ensure_installed = {
    -- programming languages
    "vim",
    "lua",
    "bash",

    -- configs
    "toml",

    -- documentation
    "markdown",
    "markdown_inline",
    "vimdoc",
    "norg",

    -- git
    "gitignore",
    "gitcommit",
  },

  sync_install = false,
  auto_install = true,
  highlight = {
    enable = true,
    --use_languagetree = true,
  },

  indent = {
    enable = true,
  },
}

require("nvim-treesitter.configs").setup(options)

---------------------------------------------------------------------------
-- Plugin: Telescope
---------------------------------------------------------------------------

-- Load Extensions
for _, ext in ipairs(require("config").tele_extensions) do
  require("telescope").load_extension(ext)
end

local options = {
  defaults = {
    mappings = {
      i = {
        ["<C-u>"] = false,
        ["<C-d>"] = false,
        ["<C-j>"] = require("telescope.actions").move_selection_next,
        ["<C-k>"] = require("telescope.actions").move_selection_previous,
      },
    },
  },
}

require("telescope").setup(options)

require("core.utils").load_mappings "telescope"

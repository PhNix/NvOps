---------------------------------------------------------------------------
-- Git integration
---------------------------------------------------------------------------
local options_gitsigns = {
  signs = {
    add = { text = "" },
    change = { text = "󰏬" },
    delete = { text = "" },
    topdelete = { text = "" },
    changedelete = { text = "󰅫" },
  },
  current_line_blame = false,
}

require("gitsigns").setup(options_gitsigns)

require("core.utils").load_mappings "git"

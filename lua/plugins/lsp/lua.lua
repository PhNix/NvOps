---------------------------------------------------------------------------
-- LSP - lua-language-server
---------------------------------------------------------------------------

local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

require("neodev").setup {
  library = {
    enabled = true,
    runtime = true,
    types = true,
    plugins = true,
  },

  lspconfig = true,
  pathStrict = true,
}

local settings = {
  Lua = {
    runtime = {
      version = "LuaJIT",
      path = runtime_path,
    },
    diagnostics = {
      globals = {
        "vim",
        "assert",
        "describe",
        "it",
        "before_each",
        "after_each",
        "pending",
        "clear",

        "G_P",
        "G_R",
      },
    },
    workspace = {
      library = {
        [vim.fn.expand "$VIMRUNTIME/lua"] = true,
      },
      checkThirdParty = true,
    },
    format = {
      enable = false,
    },
  },
}
return settings

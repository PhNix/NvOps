---------------------------------------------------------------------------
-- Custom icons
---------------------------------------------------------------------------

local options = {
  strict = true,
  color_icons = true,
  override_by_extension = require("config").icons,
}

require("nvim-web-devicons").setup(options)

---------------------------------------------------------------------------
-- Buffers
---------------------------------------------------------------------------

local options = {
  options = {
    buffer_close_icon = "",
    mode = "buffers",
    offsets = {
      {
        filetype = "NvimTree",
        text = "File Explorer",
        separator = false,
        padding = 1,
      },
    },
    diagnostics = "nvim_lsp",
    indicator = {
      icon = "  ",
      style = "icon",
    },
  },
}

require("bufferline").setup(options)

require("core.utils").load_mappings "buffers"

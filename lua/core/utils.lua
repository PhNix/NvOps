---------------------------------------------------------------------------
-- Extra utils
---------------------------------------------------------------------------

local M = {}
local merge_tb = vim.tbl_deep_extend

local function set_section_mapping(section_values, mapping_options)
  for mode, mode_values in pairs(section_values) do -- vimmode as "n" or "i"
    local default_opts = merge_tb("force", { mode = mode }, mapping_options or {})
    for keybind, mapping_info in pairs(mode_values) do -- get mapping and info about it
      -- merge defaults + argument options
      local opts = merge_tb("force", default_opts, mapping_info.opts or {})
      mapping_info.opts, opts.mode = nil, nil
      opts.desc = mapping_info[2]

      vim.keymap.set(mode, keybind, mapping_info[1], opts)
    end
  end
end

M.load_mappings = function(section, mapping_options)
  vim.schedule(function()
    local mappings = require "core.mappings"

    -- If mapping contains into core/mappings as string, not table
    if type(section) == "string" then
      mappings[section]["plugin"] = nil
      mappings = { mappings[section] }
    end

    for _, _section in pairs(mappings) do
      set_section_mapping(_section, mapping_options)
    end
  end)
end

return M

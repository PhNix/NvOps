---------------------------------------------------------------------------
-- Options
---------------------------------------------------------------------------

local options = {

  -- Clipboard
  clipboard = "unnamedplus",

  -- Number
  number = true,
  relativenumber = true,
  numberwidth = 2,
  ruler = false,

  -- Mouse
  mouse = "a",
  mousefocus = true,
  virtualedit = "block",

  -- Tabs
  expandtab = true,
  shiftwidth = 2,
  smartindent = true,
  tabstop = 2,
  softtabstop = 2,

  -- Scroll
  scrolloff = 8,

  -- String
  wrap = false,
  linebreak = true,

  -- UI
  signcolumn = "yes",
  showmode = false,
  termguicolors = true,

  -- Splits
  splitbelow = true,
  splitright = true,

  -- Interval
  updatetime = 100,

  -- Case
  ignorecase = true,
  smartcase = true,

  -- Fillchars
  fillchars = { eob = " " },

  -- Concealer for Neorg
  conceallevel = 2,

  -- Status line
  laststatus = 3,
}

local globals = {
  mapleader = " ",
  maplocalleader = "\\",
  markdown_recommended_style = 0,
  formatoptions = "qrn1",
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

for k, v in pairs(globals) do
  vim.g[k] = v
end

-- Disable some providers
for _, provider in ipairs { "node", "perl", "python3", "ruby" } do
  vim.g["loaded_" .. provider .. "_provider"] = 0
end

-- Add mason tools to path
local is_windows = vim.loop.os_uname().sysname == "Windows_NT"
vim.env.PATH = vim.fn.stdpath "data" .. "/mason/bin" .. (is_windows and ";" or ":") .. vim.env.PATH

-- Shorter messages
vim.opt.shortmess:append "cI"
vim.opt.whichwrap:append "<>[]hl"

---------------------------------------------------------------------------
-- Plugins
---------------------------------------------------------------------------
return {
  -- Code Dependencies
  { "nvim-lua/plenary.nvim" },

  -- UI Dependencies
  { "nvim-tree/nvim-web-devicons" },
  { "MunifTanjim/nui.nvim" },

  -- Mason
  {
    "williamboman/mason.nvim",
    cmd = { "Mason", "MasonInstall", "MasonInstallAll", "MasonUpdate" },
    opts = function()
      return require "plugins.mason"
    end,
    config = function(_, opts)
      require("mason").setup(opts)

      vim.api.nvim_create_user_command("MasonInstallAll", function()
        vim.cmd("MasonInstall " .. table.concat(opts.ensure_installed, " "))
      end, {})
      vim.g.mason_binaries_list = opts.ensure_installed
    end,
  },

  -- LSP
  { "neovim/nvim-lspconfig" },
  { "folke/neodev.nvim" },
  { "j-hui/fidget.nvim" },

  -- Snippets
  {
    "L3MON4D3/LuaSnip",
    dependencies = {
      { "rafamadriz/friendly-snippets" },
    },
  },

  -- Completion
  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      { "hrsh7th/cmp-nvim-lsp" },
      { "hrsh7th/cmp-nvim-lua" },
      { "hrsh7th/cmp-buffer" },
      { "hrsh7th/cmp-path" },
      { "petertriho/cmp-git" },
      { "saadparwaiz1/cmp_luasnip" },
    },
  },

  -- Diagnostics
  { "folke/trouble.nvim" },

  -- Linter and Formatter
  { "mfussenegger/nvim-lint" },
  { "mhartington/formatter.nvim" },

  -- File Explorer
  {
    "nvim-tree/nvim-tree.lua",
    init = function()
      require("core.utils").load_mappings "nvimtree"
    end,
  },

  -- Telescope
  {
    "nvim-telescope/telescope.nvim",
    dependencies = {
      { "nvim-telescope/telescope-symbols.nvim" },
      {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
        cond = vim.fn.executable "make" == 1,
      },
    },
  },

  -- Treesitter
  {
    "nvim-treesitter/nvim-treesitter",
    cmd = { "TSInstall", "TSBufEnable", "TSBufDisable", "TSModuleInfo" },
    build = ":TSUpdate",
  },

  -- Markdown Preview
  {
    "iamcco/markdown-preview.nvim",
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = function()
      vim.fn["mkdp#util#install"]()
    end,
  },

  -- Git
  { "lewis6991/gitsigns.nvim" },

  -- Neorg
  {
    "nvim-neorg/neorg",
    run = ":Neorg sync-parsers",
  },

  -- Comments
  { "numToStr/Comment.nvim" },
  { "folke/todo-comments.nvim" },

  -- Terminal
  { "akinsho/toggleterm.nvim", version = "*" },

  -- Database Manager
  {
    "tpope/vim-dadbod",
    dependencies = {
      { "kristijanhusak/vim-dadbod-ui" },
      { "kristijanhusak/vim-dadbod-completion" },
    },
  },

  -- Modern UI
  { "rcarriga/nvim-notify" },
  { "folke/noice.nvim" },
  { "nvim-lualine/lualine.nvim" },
  { "akinsho/bufferline.nvim" },

  -- Autopairs
  { "windwp/nvim-autopairs" },

  -- Colorscheme
  { "folke/tokyonight.nvim", priority = 1000 },
}

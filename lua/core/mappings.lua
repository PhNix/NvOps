---------------------------------------------------------------------------
-- Mappings
---------------------------------------------------------------------------

local M = {}

M.general = {
  i = {
    ["jj"] = { "<Esc>", "Escape from interactive mode" },
  },
  n = {
    ["<leader>w"] = { "<cmd>w<cr>", "Save the file" },
    ["<leader>q"] = { "<cmd>q<cr>", "Close the buffer" },
    ["<leader>Q"] = { "<cmd>qa!<cr>", "Close all the buffers without saving" },
    ["<cr>"] = { "<cmd>noh<cr><cr>", "Disable the highlight" },
  },
  v = {
    ["<"] = { "<gv", "Increment the indent" },
    [">"] = { ">gv", "Decrement the indent" },
  },
}

M.nvimtree = {
  n = {
    ["<leader>e"] = { "<cmd>NvimTreeFocus<CR>", "Focus NvimTree view" },
    ["<C-n>"] = { "<cmd>NvimTreeToggle<cr>", "Toggle NvimTree view" },
  },
}

M.lsp = {
  n = {
    ["<leader>rn"] = { vim.lsp.buf.rename, "Rename the symbol" },
    ["<leader>ca"] = { vim.lsp.buf.code_action, "Code action" },
    ["<leader>K"] = { vim.lsp.buf.hover, "Hover documentation" },
    ["<C-k>"] = { vim.lsp.buf.signature_help, "Signature documentation" },
    ["<leader>gd"] = { vim.lsp.buf.definition, "Goto definition" },
    ["<leader>gi"] = { vim.lsp.buf.implementation, "Goto implementation" },
    ["<leader>gD"] = { vim.lsp.buf.declaration, "Goto declaration" },
    ["<leader>D"] = { vim.lsp.buf.type_definition, "Type definition" },
  },
}

M.format = {
  n = {
    ["<leader>F"] = { "<cmd>Format<cr>", "Format the current buffer" },
  },
}

M.comment = {
  n = {
    ["<leader>/"] = {
      function()
        require("Comment.api").toggle.linewise.current()
      end,
      "Comment the string",
    },
  },
  v = {
    ["<leader>/"] = {
      "<esc><cmd>lua require('Comment.api').toggle.linewise(vim.fn.visualmode())<cr>",
      "Comment the string",
    },
  },
}

M.telescope = {
  n = {
    ["<leader>sf"] = { require("telescope.builtin").find_files, "Search the files" },
    ["<leader>sw"] = { require("telescope.builtin").grep_string, "Search the word" },
    ["<leader>sg"] = { require("telescope.builtin").live_grep, "Search by Grep" },
    ["<leader>sb"] = { require("telescope.builtin").buffers, "Search the buffers" },
    ["<leader>sd"] = { require("telescope.builtin").diagnostics, "Search the diagnostics" },
    ["<leader>sh"] = {
      require("telescope.builtin").help_tags,
      "Search the help tags",
    },
    ["<leader>sS"] = { require("telescope.builtin").git_status, "Show the git status" },
    ["<leader>sn"] = { "<cmd>lua require('telescope').extensions.notify.notify()<cr>", "Show the notifications" },
    ["<leader>st"] = { "<cmd>TodoTelescope<cr>", "Search the todo comments" },
  },
}

M.trouble = {
  n = {
    ["<leader>xx"] = { "<cmd>TroubleToggle<cr>", "Toggle the diagnostics" },
    ["<leader>xw"] = { "<cmd>TroubleToggle workspace_diagnostics<cr>", "Show the workspace diagnostics" },
    ["<leader>xd"] = { "<cmd>TroubleToggle document_diagnostics<cr>", "Show the document diagnostics" },
    ["<leader>xl"] = { "<cmd>TroubleToggle loclist<cr>", "Show the local list of diagnostics" },
    ["<leader>xq"] = { "<cmd>TroubleToggle quickfix<cr>", "Show the quickfix" },
    ["gR"] = { "<cmd>TroubleToggle lsp_references<cr>", "Show the LSP references" },
  },
}

M.buffers = {
  n = {
    ["td"] = { "<cmd>bdelete<cr>", "Close the tab" },
    ["tk"] = { "<cmd>bnext<cr>", "Next tab" },
    ["tj"] = { "<cmd>bprev<cr>", "Previous tab" },
    ["|"] = { "<cmd>vsplit<cr>", "Split right" },
    ["tg"] = { "<cmd>BufferLinePick<cr>", "Go to the specific buffer" },
    ["tc"] = { "<cmd>BufferLinePickClose<cr>", "Close the specific buffer" },
    ["\\"] = { "<cmd>split<cr>", "Split bottom" },
  },
}

M.git = {
  n = {
    ["<leader>hs"] = { "<cmd>Gitsigns stage_hunk<cr>", "Stage hunk" },
    ["<leader>hr"] = { "<cmd>Gitsigns reset_hunk<cr>", "Reset hunk" },
    ["<leader>hS"] = { package.loaded.gitsigns.stage_buffer, "Stage buffer" },
    ["<leader>ha"] = { package.loaded.gitsigns.stage_hunk, "Stage hunk" },
    ["<leader>hu"] = { package.loaded.gitsigns.undo_stage_hunk, "Undo stage hunk" },
    ["<leader>hR"] = { package.loaded.gitsigns.reset_buffer, "Reset buffer" },
    ["<leader>hp"] = { package.loaded.gitsigns.preview_hunk, "Preview hunk" },
    ["<leader>hb"] = {
      function()
        package.loaded.gitsigns.blame_line { full = true }
      end,
      "Blame line",
    },
    ["<leader>hd"] = { package.loaded.gitsigns.diffthis, "Diff this" },
    ["<leader>hD"] = {
      function()
        package.loaded.gitsigns.diffthis "~"
      end,
      "Diff this",
    },
  },
  v = {
    ["<leader>hs"] = { "<cmd>Gitsigns stage_hunk<cr>", "Stage hunk" },
    ["<leader>hr"] = { "<cmd>Gitsigns reset_hunk<cr>", "Reset hunk" },
  },
}

return M

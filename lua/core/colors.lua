---------------------------------------------------------------------------
-- Colors
---------------------------------------------------------------------------

local function SetLightHighlights() end

local function SetLightTheme()
  vim.cmd.colorscheme "tokyonight-day"
  SetLightHighlights()
end

local function SetDarkHighlights() end

local function SetDarkTheme()
  vim.cmd.colorscheme "tokyonight-night"
  SetDarkHighlights()
end

local theme = require("config").theme

if theme == "dark" then
  SetDarkTheme()
elseif theme == "light" then
  SetLightTheme()
end

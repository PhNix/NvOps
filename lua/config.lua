---------------------------------------------------------------------------
-- Configuration
---------------------------------------------------------------------------

-- IMPORTANT: DON'T DELETE PRE-CONFIGURE SETTINGS

local config = {
  theme = "dark", -- "dark" or "light"

  -- There are LSPs located here that do not require configuration
  -- If your LSPs requires specify settings. Setup it manually in plugins.lspconfig
  lsp = {
    "bashls",
  },

  linters = {
    lua = { "luacheck" },
    sh = { "shellcheck" },
  },

  formatters = {
    lua = {
      require("formatter.filetypes.lua").stylua,
    },
    sh = {
      require("formatter.filetypes.sh").shfmt,
    },
  },

  plugins = {
    orgmode = true,
    ai = false,
  },

  -- Custom icons
  icons = {
    ["norg"] = {
      icon = "",
      color = "#77AA99",
      name = "NorgMode",
    },
  },

  tele_extensions = {
    "fzf",
  },
}

return config

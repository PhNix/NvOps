---------------------------------------------------------------------------
-- Installation script for lazy.nvim and mason
---------------------------------------------------------------------------

local M = {}

M.shell_info = function(str)
  vim.cmd [[ redraw ]]
  vim.api.nvim_echo({ { str, "Bold" } }, true, {})
end

M.shell_error = function(args)
  local output = vim.fn.system(args)
  assert(vim.v.shell_error == 0, "External call failed with error code: " .. vim.v.shell_error .. "\n" .. output)
end

M.post_install = function()
  local api = vim.api
  local opt_local = vim.opt_local

  local function screen()
    -- Notes after installation
    local postinstall_info = {
      "",
      "",
      "███╗   ██╗   ██████╗  ████████╗ ███████╗ ███████╗",
      "████╗  ██║  ██╔═══██╗ ╚══██╔══╝ ██╔════╝ ██╔════╝",
      "██╔██╗ ██║  ██║   ██║    ██║    █████╗   ███████╗",
      "██║╚██╗██║  ██║   ██║    ██║    ██╔══╝   ╚════██║",
      "██║ ╚████║  ╚██████╔╝    ██║    ███████╗ ███████║",
      "",
      "",
      "  Greetings. I hope it will be convenient for you to use my assembly.",
      "",
      "  All LSP are setup in config.lsp and if needed configured in plugins/lsp, know this",
      "",
      "  All Linters and Formatters are setup in config.formatters and config.linters, know this",
      "",
      "  All Debuggers are setup in config.debug, know this",
      "",
      "  All mappings are configured in core.mappings, know this",
      "",
      "  Mason just downloads binaries, dont expect it to configure lsp automatically",
      "",
      "  This is not an assembly, but a simple configuration, I assemble it for my needs.",
      "",
      "  If you dont see any syntax highlighting, you need to install a treesitter-parser for it",
      "",
      "  Need help? Check out NeoVim community in https://reddit.com/r/neovim",
      "",
      "When markdown-preview and treesitter finish their installation, quit nvim!",
    }

    -- Create the buffer and output notes
    local buf = api.nvim_create_buf(false, true)
    vim.opt_local.filetype = "nvops_postinstall_buffer"
    api.nvim_buf_set_lines(buf, 0, -1, false, postinstall_info)
    local nvops_postinstall_buffer = api.nvim_create_namespace "nvops_postinstall_buffer"
    for i = 1, #postinstall_info do
      api.nvim_buf_add_highlight(buf, nvops_postinstall_buffer, "LazyCommit", i, 0, -1)
    end
    api.nvim_win_set_buf(0, buf)

    opt_local.buflisted = false
    opt_local.modifiable = false
    opt_local.number = false
    opt_local.list = false
    opt_local.relativenumber = false
    opt_local.wrap = false
    opt_local.cul = false
  end

  api.nvim_buf_delete(0, { force = true })

  -- Install all tools with Mason
  vim.schedule(function()
    vim.cmd "MasonInstallAll"
    local packages = table.concat(vim.g.mason_binaries_list, " ")
    require("mason-registry"):on("package:install:success", function(pkg)
      packages = string.gsub(packages, pkg.name:gsub("%-", "%%-"), "")
      if packages:match "%S" == nil then
        vim.schedule(function()
          api.nvim_buf_delete(0, { force = true })
          vim.cmd "echo '' | redraw"
          screen()
        end)
      end
    end)
  end)
end

-- Installation script to lazy.nvim and mason
M.lazy = function(lazypath)
  M.shell_info "  Installing lazy.nvim & plugins ..."
  local repo = "https://github.com/folke/lazy.nvim.git"
  M.shell_error { "git", "clone", "--filter=blob:none", "--branch=stable", repo, lazypath }
  vim.opt.rtp:prepend(lazypath)

  require "core.lazy"

  M.post_install()
end

return M

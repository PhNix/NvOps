## NvOps - Neovim configuration for DevOps

### Required programs

- gcc
- make
- node(for some plugins and mason tools)
- ripgrep
- fzf
- ollama(for AI integration)

### Installation

```sh
git clone https://gitea.com/PhNix/NvOps.git
cd nvops
./install.sh
```

### Mappings

In my configuration I realize different mappings system(nvchad system, but without some trash)

In file **core.mappings** all mappings are stored in this format:

```lua
M.nvimtree = {
  n = {
    ["<leader>e"] = { "<cmd> NvimTreeToggle <CR>", "Toggle nvimtree" },
  },
}
```
Where, instead of nvimtree, any name of the group is written (convenient for you).
***n*** is a normal mod, that is, for example, you can put ***i*** or ***v*** instead.

And then comes the mapping itself, in the following format:
`[mapping] = { function, description }`

After you can enable this mappings:
`require("core.utils").load_mappings "nvimtree"`

## Todo's

- [X] Installer
- [X] User config(**lua/config**)
- [X] More comfortable mappings system(like nvchad, but without trash)
- [X] Light and Dark theme support(**tokyo-night** and **tokyo-day**)
- [ ] LSP and others
  - [X] mason.nvim
  - [X] nvim-lspconfig
  - [X] nvim-cmp
  - [X] formatter
  - [X] nvim-lint
  - [ ] nvim-dap
- [X] Diagnostics
  - [X] trouble.nvim
- [X] Neovim setup for make notes. It's a markdown, org and neorg.
  - [X] markdown-preview.nvim
  - [X] nvim-neorg
- [X] Modern UI
  - [X] noice.nvim
  - [X] nvim-notify
  - [X] Telescope
- [X] Database support(vim-dadbod).
- [ ] Good git integration
  - [ ] lazygit 
  - [X] gitsings.nvim
  - [ ] Github Actions 
  - [ ] Gitlab CI/CD
- [ ] Docker support
  - [ ] lazydocker
  - [ ] Docker compose LSP
  - [ ] Dockerfile LSP
- [ ] Greetings screen(where we use `nvim` without filename argument)
- [X] Statusline
- [X] Buffers as tabs(bufferline.nvim)
- [ ] Languages(without Lua)
  - [X] Bash
  - [ ] Python
  - [ ] Golang
  - [ ] Yaml
  - [ ] Json
- [X] Integrated terminal
- [ ] Create the documentation
  - [ ] About Ollama AI
  - [ ] About OrgMode and Neorg plugin
- [ ] Nix integration

---------------------------------------------------------------------------
-- Initial instructions
---------------------------------------------------------------------------

-- Load options
require "core.options"
require "core.autocmds"

-- Load lazy.nvim
local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  require("installer").lazy(lazypath)
end
vim.opt.rtp:prepend(lazypath)
require "core.lazy"

require "core.colors"

-- TODO: Sort plugins by loader priority

-- Plugins(except Mason, because it need for installation script)

-- UI
require "plugins.devicons"
require "plugins.notify"
require "plugins.noice"
require "plugins.statusline"
require "plugins.file-browser" -- NvimTree
require "plugins.buffers"
require "plugins.terminal"

-- Code
require "plugins.treesitter"
require "plugins.lspconfig"
require "plugins.cmp"
require "plugins.trouble"
require "plugins.format"

-- Other
require "plugins.autopairs"
require "plugins.comments"
require "plugins.telescope"
require "plugins.git"

-- Because it'v vim plugin, it's not have setup function
require("plugins.dadbod").setup()

local plugins = require("config").plugins
-- Extra plugins
if plugins.orgmode == true then
  require "plugins.org-mode"
end

require("core.utils").load_mappings "general"
